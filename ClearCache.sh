#!/bin/bash
email=
zone_id=
key= 

curl -vvv -X POST "https://api.cloudflare.com/client/v4/zones/${zone_id}/purge_cache" \
     -H "X-Auth-Email: ${email}" \
     -H "X-Auth-Key: ${key}" \
     -H "Content-Type: application/json" \
     --data '{"purge_everything":true}'
