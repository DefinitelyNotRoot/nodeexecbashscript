#!/bin/bash
rm -f dev.sql;
lf=/root/devtoqa.lock 

[ -f $lf ] && echo "Already running!" && exit 

touch $lf
echo -e "Backup the dev db..."
mysqldump -u root dev > /root/dev.sql
echo -e "Backup dev db: completed!"
echo -e "Backup the current qa instance..."
mysqldump -u root app > /root/app_$(date -d "today" +"%Y%m%d%H%M").sql
echo -e "Backup qa db: completed!"
echo -e "Loading the dev db"
mysql -u root app < /root/dev.sql
echo -e "Dev to QA: Database migrated!"

echo "Database updated!"
rm -f $lf
