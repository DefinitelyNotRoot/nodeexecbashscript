const http = require('http')
const util = require('util')
const exec = util.promisify(require('child_process').exec)
const token = '<enter_your_token>'
const taskList = [
  { path: '/updatedb', body: 'QA', method: 'POST', cmd: 'bash CopyDB_DEVtoQA.sh', token: token },
  { path: '/updatesrc', body: 'QA', method: 'POST', cmd: 'bash SourceCodeUpdate_QA.sh', token: token },
  { path: '/clearcache', body: 'QA', method: 'POST', cmd: 'bash ClearCache.sh', token: token },
]

const server = http.createServer(function (request, response) {
  var body = ''
  request.on('data', function (data) {
    body += data
  }).on('end', function () {
    var searchedItem = {
      path: request.url,
      body: body,
      method: request.method,
      token: token
    }

    function search(taskList) {
      return Object.keys(this).every((key) => taskList[key] === this[key])
    }
    
    var result = taskList.filter(search, searchedItem)
    if (Object.keys(result).length != 0) {
      async function execPromise(command) {
        const { stdout, stderr } = await exec(command);
        return new Promise((resolve, reject) => {
          if (stderr) return reject(stderr)
          resolve(stdout)
        })
      }
      execPromise(result[0]['cmd']).then(res => {
        response.writeHead(200, { 'Content-Type': 'text/html' })
        response.end(res)
      }).catch(err => {
        response.writeHead(400, { 'Content-Type': 'text/html' })
        response.end(err)
      })
    } else {
      response.writeHead(400, { 'Content-Type': 'text/html' })
      response.end("ERROR")
    }

  })
})

const port = 3000
const host = '0.0.0.0'
server.listen(port, host)
console.log(`Listening at http://${host}:${port}`)
